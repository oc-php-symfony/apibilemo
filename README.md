# APIBileMo

BileMo project, OpenClassrooms Backend/Symfony Developer path, project #7

##Requirements

-   Apache 2.4 or Nginx 1.13
-   Php 7.2
-   MySQL 5.7
-   Composer 1.9

##Without docker

-   clone project in your apache or nginx server root directory
-   run `composer install` to install vendors
-   edit DSN in .env to match your db access `DATABASE_URL=mysql://<<user>>:<<password>>@<<db_host>>:<<db_port>>/<<db_name>>`
-   run `doctrine:database:create` to create the database (not needed if you use the docker container)
-   run `doctrine:migrations:migrate` to create database tables
-   run `doctrine:fixtures:load` to load fixtures

NB: prefix command with 'php bin/console' or 'bin/console' depending on your OS and setup

##With docker
-   clone into docker container, to "bilemo" folder (https://gitlab.com/DamienAdam/dockerapibilemo.git)
-   run `sh runner php dev` from container folder
-   run `php bin/console composer install` to install vendors
-   You should have to edit the .env DSN if you use the docker package provided
-   run `php bin/console doctrine:database:create` to create the database (not needed if you use the docker container)
-   run `php bin/console doctrine:migrations:migrate` to create database tables
-   run `php bin/console doctrine:fixtures:load` to load fixtures
-   go to 127.0.0.1/api for docs

nb: with docker container, phpmyadmin available at 127.0.0.1:8000 for your convenience
