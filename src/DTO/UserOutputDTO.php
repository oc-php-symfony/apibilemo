<?php

namespace App\DTO;

use App\Entity\Customer;

class UserOutputDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $email;

    /**
     * @var Customer
     */
    public $customer;

}
