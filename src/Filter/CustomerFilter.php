<?php

namespace App\Filter;

use App\Annotation\CustomerAware;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Doctrine\Common\Annotations\Reader;

final class CustomerFilter extends SQLFilter
{
    private $reader;

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if (null === $this->reader) {
            throw new \RuntimeException(sprintf('An annotation reader must be provided. Be sure to call "%s::setAnnotationReader()".', __CLASS__));
        }

        $customerAware = $this->reader->getClassAnnotation($targetEntity->getReflectionClass(), CustomerAware::class);

        if (!$customerAware) {
            return '';
        }

        $fieldName = $customerAware->customerFieldName;


        try {
            $userCustomer = $this->getParameter('id');
        } catch (\InvalidArgumentException $e) {
            return '';
        }

        if (empty($fieldName) || empty($userCustomer)) {
            return '';
        }


        return sprintf('%s.%s = %s', $targetTableAlias, $fieldName, $userCustomer);
    }

    public function setAnnotationReader(Reader $reader): void
    {
        $this->reader = $reader;
    }
}