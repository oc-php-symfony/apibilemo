<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Phone;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $fake = Factory::create();

        for ($c = 0; $c < 5; $c++) {

            $customer = new Customer();
            $customer->setName($fake->company);

            for ($i = 0; $i < 10; $i++) {
                $user = new User();
                $passHash = $this->encoder->encodePassword($user, 'password');

                $user
                    ->setEmail($fake->email)
                    ->setPassword($passHash)
                    ->setCustomer($customer);

                $manager->persist($user);
            }

            $manager->persist($customer);
            $manager->flush();
        }

        for ($i = 0; $i < rand(40, 125); $i++) {
            $phone = (new Phone())
                ->setModel($fake->streetName)
                ->setDescription($fake->text(320))
                ->setMsrp($fake->randomFloat(2, 120, 1250.25));

            $manager->persist($phone);

            if ($i%10 === 0) {
                $manager->flush();
            }
        }
        $manager->flush();

    }
}
