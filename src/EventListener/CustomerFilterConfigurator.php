<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Annotations\Reader;

final class CustomerFilterConfigurator
{
    private $em;
    private $security;
    private $reader;
    private $request;

    public function __construct(EntityManagerInterface $em, Security $security, Reader $reader, RequestStack $request)
    {
        $this->em = $em;
        $this->security = $security;
        $this->reader = $reader;
        $this->request = $request;
    }

    public function onKernelRequest(): void
    {
        if (!$user = $this->getUser()) {

            return;

            //throw new \RuntimeException('There is no authenticated user.');
        }

        //when we get a user we want to block with voter (later in request processing) rather than filtering

        $bypass_method = ["get", "put", "patch", "delete"];
        $bypassed = in_array($this->request->getCurrentRequest()->get('_api_item_operation_name'), $bypass_method);

        if ($bypassed) {
            return;
        }

        $filter = $this->em->getFilters()->enable('customer_filter');
        $filter->setParameter('id', $user->getCustomer()->getId());
        $filter->setAnnotationReader($this->reader);
    }

    private function getUser(): ?UserInterface
    {
        $user = $this->security->getUser();
        return $user instanceof UserInterface ? $user : null;
    }
}